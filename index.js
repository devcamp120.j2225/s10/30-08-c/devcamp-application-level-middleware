const express = require('express');

const app = new express();

const port = 8000;

app.use((request, response, next) => {
    console.log(new Date());
    next();
});

app.get('/', (request, response, next) => {
    console.log(request.method);
    next();
});

app.get('/', (request, response) => { 
    let today = new Date();
    response.status(200).json({
        message:`Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    });
})

app.post('/post', (request, response) => {
    response.status(200).json({
        message:'Post method'
    });
});

app.put('/put', (request, response) => {
    response.status(200).json({
        message:'Post method'
    });
});

app.listen(port, () => {
    console.log(`App listen on port ${port}!`);
});